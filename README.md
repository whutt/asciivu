# asciivu
Usage:
```
./asciivu.py file
```

Converts IRC formatting codes to ANSI terminal codes, then displays the result.

For Windows users: use [Ansicon](https://github.com/adoxa/ansicon) or [mintty](https://mintty.github.io/); the Windows console has no ANSI code support.
