#!/usr/bin/env python3
import sys, re

def Error(s):
    print("%s: %s" % (sys.argv[0], s))
    sys.exit(1)

if len(sys.argv) < 2:
    Error("need a file")

Encodings = ["utf-8", "windows-1252"]
Lines = None
for enc in Encodings:
    try:
        Lines = [l.rstrip('\r\n') for l in open(sys.argv[1], encoding=enc).readlines()]
        break
    except ValueError:
        continue
    except Exception as e:
        Error(e)

if not Lines:
    Error("%s: unknown encoding" % sys.argv[1])

ColorLookup = ['15','0','4','2','9','1','5','214','11','10','6','14','12','13','8','7']

for l in Lines:
    l = re.sub('\003(\d{1,2}),(\d{1,2})', lambda m: '\033[38;5;%sm\033[48;5;%sm' % (ColorLookup[int(m[1]) % 16], ColorLookup[int(m[2]) % 16]), l)
    l = re.sub('\003(\d{1,2})', lambda m: '\033[38;5;%sm' % ColorLookup[int(m[1]) % 16], l)
    l = re.sub('(\017|\003)', '\033[0m', l)
    
    print(l + '\033[0m')

